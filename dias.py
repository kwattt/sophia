import discord
from discord.ext import commands, tasks
import random
import aiohttp
import json
from datetime import datetime
import asyncio
import parse
import time
import pandas as pd
import numpy as np

with open("data.json", encoding='utf8') as json_file:
    c_data = json.load(json_file)

def load_default():
    with open('ndefaults.json', encoding='utf8') as json_file:
        r_data = json.load(json_file)

    global status_s 
    status_s= r_data['status_s']
    global stalk_response
    stalk_response = r_data['stalk_response']
    global join_msg 
    join_msg = r_data['join_msg']
    global oraculo_msg 
    oraculo_msg = r_data['oraculo_msg']
    global stalk_users 
    stalk_users = r_data['stalk_users']

    global s_reddits 
    s_reddits = r_data['s_reddits']
    global s_redditsmeme 
    s_redditsmeme = r_data['s_redditsmeme']


    global d_memes
    global d_wholesome
    d_memes = []
    d_wholesome = []
load_default()

client = commands.Bot(command_prefix='!', description="Sophia MK2, bot en honor a la anterior sophia que fue abandonada en el olvido.")

@client.event
async def on_command_error(ctx, error):

    if isinstance(error, commands.UserInputError):
        return
    elif isinstance(error, commands.CommandNotFound):
        return
    else: 
        print(error)

@client.event
async def on_command_completion(ctx):
    print('%s usó el comando %s'%(ctx.author.name, ctx.command))

enabled = 1
active = 0

@client.command(description='TARDES YA')
async def diasya(ctx):
    import matplotlib.pyplot as plt

    global enabled
    global active
    
    if enabled == 0:
        await ctx.send("El comando este no está activado, será xq matas la API y destrozas la ram!?¡!¡?")
        return 0

    if active == 1:
        await ctx.send("Solo puede ser usado por un usuario a la vez.")
        return 0

    active = 1

    chlist = [681746292950564882,609254980448288772,620737236068990985,609911162359906317]

    tmsg = 200000
    tiempoe = (tmsg*555) / 200000

    await ctx.send("Tiempo estimado para el cálculo: %i segundos o %i minutos"%(tiempoe, tiempoe//60))
    print(tiempoe)
    s1 = int(time.time())

    data = {}

    tcounter = 0
    totalmess = 0
    ttardes = 0
    tdias = 0
    tbuenas = 0
    tholas = 0

    ptardes = []
    pbuenas = []
    pdias = []
    pholas = []

    for x in chlist:
        channel = client.get_channel(x)
        messages = await channel.history(limit=tmsg).flatten()
        print("total mensajes:", len(messages))
        for mess in messages:
            if mess.author == ctx.author:
                tcounter+=1
                msgc = mess.content.lower()
                date = "%i-%i"%(mess.created_at.year, mess.created_at.month)

                if "dias ya" in msgc or "días ya" in msgc:
                    tdias+=1
                    print("días en", mess.created_at)
                    pdias.append(mess.created_at.hour)
                    try:
                        data[date] += 1
                    except:
                        data[date] = 1

                if "tardes ya" in msgc:
                    ttardes+=1
                    ptardes.append(mess.created_at.hour)
                    print("tardes en", mess.created_at)
                    try:
                        data[date] += 1
                    except:
                        data[date] = 1

                if "buenas buenas" in msgc:
                    tbuenas+=1
                    pbuenas.append(mess.created_at.hour)
                    print("buenas en", mess.created_at)
                    try:
                        data[date] += 1
                    except:
                        data[date] = 1

                if "hola" in msgc:
                    tholas+=1
                    pholas.append(mess.created_at.hour)
                    print("holas en", mess.created_at)
                    try:
                        data[date] += 1
                    except:
                        data[date] = 1

        totalmess+= len(messages)
    
    s2 = int(time.time())

    print("total = ",s2-s1)

    await ctx.send("Tiempo transcurrido %i segundos"%(s2-s1))

    print(data)

    plt.style.use('dark_background')

    months = []
    values = []
    for x in sorted(data):
        months.append(x)
        values.append(data[x])
    ypos = np.arange(len(months))

    plt.bar(ypos, values, color=(0.2, 0.4, 0.6, 0.6))
    plt.xticks(ypos, months)

    plt.title("Saludos por mes de %s"%ctx.author.nick)

    plt.savefig("foobar2.png")

    plt.title("Saludos por hora de %s"%ctx.author.nick)

    hours = []
    values = []
    for x in range(24):
        hours.append(x)
        values.append(0)

    for x in pbuenas:        
        values[x] += 1
    for x in pholas:        
        values[x] += 1
    for x in ptardes:        
        values[x] += 1
    for x in pdias:        
        values[x] += 1

    ypos = np.arange(len(hours))

    plt.bar(ypos, values, color=(0.2, 0.4, 0.6, 0.6))
    plt.xticks(ypos, hours)
    plt.title("Saludos por hora de %s"%ctx.author.nick)

    plt.savefig("foobar3.png")

    await ctx.send("Bien %s, hay un total de %i mensajes en los %i canales seleccionados"%(ctx.author.nick, totalmess, len(chlist)))
    await ctx.send("De los %i mensajes, %i son tuyos"%(totalmess, tcounter))
    await ctx.send("Ahora tus modales putito")

    tsaludos = tdias+ttardes+tbuenas+tholas

    await ctx.send("De estos %i mensajes tuyos, %i son saludos."%(tcounter, tsaludos)) 

    await ctx.send("Y de estos saludos tenemos que:")
    await ctx.send("%i son días ya"%tdias)
    await ctx.send("%i son tardes ya"%ttardes)
    await ctx.send("%i son buenas buenas"%tbuenas)
    await ctx.send("%i son holas"%tholas)
    await ctx.send("Y si no me crees checa las graficas morro.")

    await ctx.send(file=discord.File('foobar2.png'))
    await ctx.send(file=discord.File('foobar3.png'))

    await ctx.send("Gracias por asistir a mi ted talk")

    print("total de %i mensajes "%totalmess)
    print("total de %i mensajes de user"%tcounter)
    print("total de %i dias"%tdias)
    print("total de %i tardes"%ttardes)
    print("total de %i buenas"%tbuenas)

    ptardes = []
    pbuenas = []
    pdias = []
    pholas = []
    data = {}

    active = 0

@client.event
async def on_ready():
    print('Bot cargado. (%s) '%client.user)   
    print("")
    print("Invite link: https://discordapp.com/api/oauth2/authorize?client_id="+str(client.user.id)+"&permissions=4189264&scope=bot")
    print("")
    print('Load latency: %f ms'%round(client.latency*1000))
    print('')

client.run(c_data["discord"])