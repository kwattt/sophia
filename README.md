# S.O.P.H.I.A MK2

S.O.P.H.I.A MK2 es un bot de discord con funcionalidades mixtas, perfecto para un servidor de un streamer.

## Prerequisitos

1. python-twitch-client - https://github.com/tsifrer/python-twitch-client
2. discord.py - https://github.com/Rapptz/discord.py
3. praw - https://github.com/praw-dev/praw

