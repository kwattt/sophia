from discord.ext import commands
import json

# status & config

cogs = ["cogs.random_talk",
        "cogs.reddit",
        "cogs.twitch",
        "cogs.bot",
        "cogs.bday",
        "cogs.notes",
        "cogs.maths"]

with open("data.json", encoding='utf8') as json_file:
    key = json.load(json_file)['discord']


client = commands.Bot(bot=True, reconnect=True, command_prefix='!',
                      description='''
                      Sophia MK2, bot en honor a la anterior
                       sophia que fue abandonada en el olvido.
                      ''')


if __name__ == "__main__":
    for cog in cogs:
        try:
            client.load_extension(cog)
            print("{} cargado correctamente.".format(cog))

        except Exception as e:
            print("No fue posible cargar el cog %s" % cog)
            print("{}: {}".format(type(e).__name__, e))

client.run(key)
