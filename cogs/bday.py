from discord.ext import commands, tasks
import os
import json
from datetime import datetime
from pytz import timezone


class BDAY(commands.Cog):

    def __init__(self, client):
        self.client = client
        with open(os.path.dirname(__file__) + "/../bdays.json",
                  encoding="utf-8") as json_file:
            self.bdays = json.load(json_file)

    @commands.Cog.listener()
    async def on_ready(self):
        self.vueltas.start()

    @tasks.loop(seconds=3600)
    async def vueltas(self):

        ctime = datetime.now(timezone("America/Mexico_City"))
        for persona in self.bdays:
            u_data = self.bdays[persona]
            if ctime.year != u_data['lasty']:
                if ctime.month == u_data['month']:
                    if ctime.day == u_data['day']:

                        u_data['lasty'] = ctime.year
                        self.bdays[persona] = u_data
                        with open(os.path.dirname(__file__) + "/../bdays.json",
                                  'w') as outfile:
                            json.dump(self.bdays, outfile, default=str,
                                      sort_keys=True, indent=4)

                        uInfo = self.client.get_user(int(persona))
                        channel = self.client.get_channel(681746292950564882)
                        await channel.send('Hoy es el cumpleaños del {}! :cake: <a:hyped02:605922840679677952> @here'.format(uInfo.name))



def setup(client):
    client.add_cog(BDAY(client))
