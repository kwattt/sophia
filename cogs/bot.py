from discord.ext import commands, tasks
from discord import Game, Embed
from random import choice
import os
import json


class BOT(commands.Cog):

    def __init__(self, client):
        self.client = client
        with open(os.path.dirname(__file__) + "/../ndefaults.json",
                  encoding="utf-8") as json_file:
            jfile = json.load(json_file)

            self.status_s = jfile['status_s']
            self.join_msg = jfile['join_msg']

    @commands.Cog.listener()
    async def on_ready(self):
        print('Terminado en: %f ms' % round(self.client.latency * 1000))
        self.randomStatus.start()

    @tasks.loop(seconds=180)
    async def randomStatus(self):
        status = choice(self.status_s)
        await self.client.change_presence(activity=Game(status))

    @commands.Cog.listener()
    async def on_command_completion(self, ctx):
        print('%s usó el comando %s' % (ctx.author.name, ctx.command))

    @commands.Cog.listener()
    async def on_member_join(self, member):
        if member.guild.id == 605773332402405386:
            member_m = '<@%i>' % member.id
            msg = choice(self.join_msg)
            msg = msg % member_m
            print('%s ingresó al servidor' % member.nick)
            channel = self.client.get_channel(605774761737322517)
            await channel.send(msg)

    @commands.Cog.listener()
    async def on_member_remove(self, member):
        if member.guild.id == 605773332402405386:
            member_m = '<@%i>' % member.id
            msg = 'Nos vemos %s :broken_heart:' % member_m
            print('%s salió del servidor' % member.nick)
            channel = self.client.get_channel(605774761737322517)
            await channel.send(msg)

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):
        if isinstance(error, commands.UserInputError):
            return
        elif isinstance(error, commands.CommandNotFound):
            return
        else:
            print(error)

    @commands.command(description='Info del bot;)')
    async def info(self, ctx):
        await ctx.send('''
Soy la sucesión de <@605780919995072524>, que dejó de funcionar el \
10 de septiembre de 2019, \
ahora soy mantenida por el kwat.
        ''')

    @commands.command(description='Comandos disponibles',
                      aliases=['comandos', 'ayuda', 'cmds'])
    async def halp(self, ctx):
        embed = Embed(title='Comandos',

                      description='''
**ayuda:** Muestra este mensaje
**avisar:**Te recuerdo algo! [arg "en" arg_tiempo]
**wholesome:** Algo wholesome!
**dank:** SHOW ME YOUR MEMES
**moneda:** ¿Cara o Cruz?
**oráculo:** ¿Qué digo yo?
**twitch:** [arg] > informaciónde un streamer en vivo
**info:** Muestra mi info(ó no)
''')

        await ctx.send(embed=embed)


def setup(client):
    client.add_cog(BOT(client))
