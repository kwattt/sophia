from discord.ext import commands
import json
import datetime as dt
import time
from discord import Embed

Media = {}

def checkKey(key, dict):       
    if key in dict: 
        return 1
    else: 
        return 0

with open('notes.json', encoding='utf8') as json_file:
    Media = json.load(json_file)

def saveFile():
    with open('notes.json', 'w') as outfile:
        json.dump(Media, outfile, default=str, sort_keys=True, indent=4)

def unix_to_dt(value):    
    return dt.datetime.fromtimestamp(value)

class NOTES(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command()
    async def cnote(self, ctx, *args):
        global Media
        if args:
            aid = str(ctx.author.id)
            
            cname = ' '.join(args).lower()
            
            if not checkKey(aid, Media):  
                Media[aid] = {"default": cname}

            ucont = Media[aid]
            
            if(checkKey(cname, ucont)):
                await ctx.send("Ya tienes una nota con este nombre.")
                return
            else:
                # 0: date - 1: content
                ucont["default"] = cname
                ucont[cname] = [int(time.time()),""]
                await ctx.send("Se creó la nota %s"%cname)

            Media[aid] = ucont
        saveFile()

    @commands.command()
    async def notes(self, ctx):
        aid = str(ctx.author.id)
        if checkKey(aid, Media):
            df = Media[aid]["default"]
            val = ""
            for x in Media[aid]:
                if x != "default":
                    cer = Media[aid]
                    values = cer[x]
                    cer = unix_to_dt(values[0])
                    if x == df:
                        val += "-> **[%i-%i-%i]** %s\n"%(cer.year, cer.month, cer.day, x)
                    else:
                        val += "**[%i-%i-%i]** %s\n"%(cer.year, cer.month, cer.day, x)

            embed=Embed(title=ctx.author.nick, description=val, color=0x00874b)
            await ctx.send(embed=embed)
        else:
            await ctx.send("No tienes notas")

    @commands.command()
    async def anote(self, ctx, *, args):
        aid = str(ctx.author.id)
        if args:
            if checkKey(aid, Media):
                notas = Media[aid]
                newarg = notas[notas['default']][1] + '\n'+args
                if len(newarg) > 1800:
                    ctx.send("Esta nota supera los 1800 caracteres")
                    return
                notas[notas['default']][1] = newarg
                Media[aid] = notas
                saveFile()
                await ctx.send("Nota actualizada")

    @commands.command()
    async def note(self, ctx, *args):
        aid = str(ctx.author.id)
        if args:
            cname = ' '.join(args)
            if cname == 'default':
                return
            if checkKey(aid, Media):
                notas = Media[aid]
                if checkKey(cname, notas):
                    contenido = notas[cname]

                    embed=Embed(title=cname, description=contenido[1], color=0x00921a)
                    contenidox = unix_to_dt(contenido[0]) 
                    embed.set_footer(text="%i-%i-%i %i:%i (len: %i)"%(contenidox.year,contenidox.month,contenidox.day, contenidox.hour, contenidox.minute, len(contenido[1])))
                    await ctx.send(embed=embed)    

    @commands.command()
    async def enote(self, ctx, *, args):
        global Media
        aid = str(ctx.author.id)
        if args:
            if len(args) > 1800:
                await ctx.send("No se pueden superar los 1800 caracteres")
                return
            if checkKey(aid, Media):
                notas = Media[aid]
                notas[notas['default']][1] = args 
                Media[aid] = notas
                saveFile()
                await ctx.send("Nota actualizada")
                await ctx.message.delete()
            else:
                await ctx.send("No tienes ninguna nota.")

    @commands.command()
    async def dnote(self, ctx, *args):
        global Media
        aid = str(ctx.author.id)
        if len(args) > 1:
            cname = ' '.join(args).lower()
            if cname == 'default':
                return
            if checkKey(aid, Media):
                notas = Media[aid]
                if checkKey(cname, notas):
                    del notas[cname]
                    await ctx.send("Eliminada la nota %s"%cname)
                else:
                    await ctx.send("Esta nota no existe")
                Media[aid] = notas
            else: 
                await ctx.send("Esta nota no existe")
            saveFile()

    @commands.command()
    async def mnote(self, ctx):
        global Media
        aid = str(ctx.author.id)
        if checkKey(aid, Media):
            notas = Media[aid]
            embed=Embed(title=notas['default'], description= notas[notas['default']][1], color=0x00921a)
            contenidox = unix_to_dt( notas[notas['default']][0]) 
            embed.set_footer(text="%i-%i-%i %i:%i (len: %i)"%(contenidox.year,contenidox.month,contenidox.day, contenidox.hour, contenidox.minute, len( notas[notas['default']][1])))
            await ctx.send(embed=embed)    
            await ctx.message.delete()

        else:
            await ctx.send("No tienes ninguna nota.")        

    @commands.command()
    async def snote(self, ctx, *args):
        global Media
        aid = str(ctx.author.id)
        if args:
            cname = ' '.join(args).lower()
            if cname == 'default':
                return
            if checkKey(aid, Media):
                notas = Media[aid]
                if checkKey(cname, notas):
                    notas["default"] = cname
                    await ctx.send("Editando la nota %s"%cname)
                else:
                    await ctx.send("Esta nota no existe")
                Media[aid] = notas

            else: 
                await ctx.send("Esta nota no existe")
            saveFile()


def setup(client):
    client.add_cog(NOTES(client))