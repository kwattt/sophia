from discord.ext import commands
import random
import json
import os


class TALK(commands.Cog):

    def __init__(self, client):
        self.client = client

        with open(os.path.dirname(__file__) + "/../ndefaults.json",
                  encoding="utf-8") as jfile:

            tdata = json.load(jfile)

            self.stalk_users = tdata["stalk_users"]
            self.stalk_response = tdata["stalk_response"]
            self.oraculo_msg = tdata["oraculo_msg"]
            self.admin_r = tdata["admin_r"]

    @commands.command(description='¿Cara o Cruz?')
    async def moneda(self, ctx):
        numberm = random.randrange(1, 101, 1)
        if numberm < 50:
            await ctx.send('Ha salido cara')
        elif numberm < 99:
            await ctx.send('Ha salido cruz')
        else:
            await ctx.send('Ha salido... ¿DE CANTO?, qué suerte.')

    @commands.command(description='Invitación del servidor',
                      aliases=['invite', 'invitacion'])
    async def invitar(self, ctx):
        await ctx.send('https://discordapp.com/invite/FaZMs54')

    @commands.command(description='Un mensaje random', aliases=['oráculo'])
    async def oraculo(self, ctx):
        msg_s = random.choice(self.oraculo_msg)
        await ctx.send(msg_s)

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.author.id in self.stalk_users:
            if random.randrange(1, 250) == 70:
                await message.channel.send(random.choice(self.stalk_response))
        if "admin" in message.content or "mod" in message.content:
            if random.randrange(1, 120) == 70:
                await message.channel.send(random.choice(self.admin_r))


def setup(client):
    client.add_cog(TALK(client))
