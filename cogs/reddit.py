from discord.ext import commands, tasks
from discord import Embed
import praw
import json
import os
import random

with open(os.path.dirname(__file__) + "/../data.json",
          encoding="utf-8") as json_file:
    jfile = json.load(json_file)

    reddit = praw.Reddit(client_id=jfile["reddit_id"],
                         client_secret=jfile["reddit_s"],
                         user_agent='sphia1')

with open(os.path.dirname(__file__) + "/../ndefaults.json",
          encoding="utf-8") as json_file:
    jfile = json.load(json_file)

    s_redditsmeme = jfile['s_redditsmeme']
    s_reddits = jfile['s_reddits']

d_memes = None
d_wholesome = None


class REDDIT(commands.Cog):

    def __init__(self, client):
        self.client = client

    @tasks.loop(seconds=1800)
    async def update_reddit(self):
        global d_memes
        global d_wholesome

        s_reddit = '+'.join(s_redditsmeme)
        d_memes = list(filter(lambda x: x.is_self is False
                              and x.over_18 is False,
                              reddit.subreddit(s_reddit).hot(limit=100)))

        s_reddit = '+'.join(s_reddits)
        d_wholesome = list(filter(lambda x: x.is_self is False
                                  and x.over_18 is False,
                                  reddit.subreddit(s_reddit).hot(limit=100)))

    @commands.Cog.listener()
    async def on_ready(self):
        self.update_reddit.start()

    @commands.command(description='Imagenes wholesome<3', aliases=['cute'])
    async def wholesome(self, ctx, arg=None):

        if d_wholesome:
            if not arg:

                d_wholesomex = random.choice(d_wholesome)

                embed = Embed(title=d_wholesomex.title, url=d_wholesomex.url)
                embed.set_author(name=d_wholesomex.author.name)
                embed.set_image(url=d_wholesomex.url)
                embed.set_footer(text=d_wholesomex.url)
                await ctx.send(embed=embed)

            else:
                arg = arg.lower()

                d_wholesomex = list(filter(lambda x: x.is_self is False and
                                           x.over_18 is False and str(arg)
                                           in str(x.title).lower(),
                                           d_wholesome))

                if d_wholesomex:
                    d_wholesomex = random.choice(d_wholesomex)
                    embed = Embed(title=d_wholesomex.title,
                                  url=d_wholesomex.url)
                    embed.set_author(name=d_wholesomex.author.name)
                    embed.set_image(url=d_wholesomex.url)
                    embed.set_footer(text=d_wholesomex.url)
                    await ctx.send(embed=embed)
                else:
                    await ctx.send('No se encontró ningún post :(')

    @commands.command(description='Imagenes memesticas:vibe:',
                      aliases=['dank'])
    async def meme(self, ctx, arg=None):
        global d_memes

        if d_memes:
            if not arg:

                d_memesx = random.choice(d_memes)

                embed = Embed(title=d_memesx.title, url=d_memesx.url)
                embed.set_author(name=d_memesx.author.name)
                embed.set_image(url=d_memesx.url)
                embed.set_footer(text=d_memesx.url)
                await ctx.send(embed=embed)

            else:
                arg = arg.lower()

                d_memesx = list(filter(lambda x: x.is_self is False and
                                       x.over_18 is False and str(arg)
                                       in str(x.title).lower(),
                                       d_memes))

                if d_memesx:
                    d_memesx = random.choice(d_memesx)

                    embed = Embed(title=d_memesx.title, url=d_memesx.url)
                    embed.set_author(name=d_memesx.author.name)
                    embed.set_image(url=d_memesx.url)
                    embed.set_footer(text=d_memesx.url)
                    await ctx.send(embed=embed)
                else:
                    await ctx.send('No se encontró ningún post :(')


def setup(client):
    client.add_cog(REDDIT(client))
