from discord.ext import commands, tasks
from discord import Embed
import json
import os
from twitch import TwitchClient


class TWITCH(commands.Cog):

    def __init__(self, client):
        self.client = client
        self.isStreamOnline = False

        with open(os.path.dirname(__file__) + "/../data.json",
                  encoding="utf-8") as json_file:
            jfile = json.load(json_file)

            self.t_client = TwitchClient(client_id=jfile['twitch'])

    @commands.Cog.listener()
    async def on_ready(self):
        self.stream.start()

    @tasks.loop(seconds=90)
    async def stream(self):

        user = ['juggerwicho115']
        user_n = self.t_client.users.translate_usernames_to_ids(user)
        user_n = user_n[0]
        info = self.t_client.streams.get_stream_by_user(user_n.id)

        if info:
            if self.isStreamOnline is False:
                channel = self.client.get_channel(610840540602302465)
                self.isStreamOnline = True
                await channel.send('''
                @everyone ¡Wicho inició stream!
                 https://www.twitch.tv/JuggerWicho115''')

        else:
            if self.isStreamOnline is True:
                channel = self.client.get_channel(610840540602302465)
                await channel.send('Se acabó el stream morros')
                self.isStreamOnline = False

    @commands.command(description='Ver si un streamer está online')
    async def twitch(self, ctx, arg):
        if arg:
            try:
                user_n = self.t_client.users.translate_usernames_to_ids([arg])
                user_n = user_n[0]
                info = self.t_client.streams.get_stream_by_user(user_n.id)

                if info:
                    viewer = info.viewers

                    info = info.channel
                    desc = "%i viewers\n%s" % (viewer, info.game)

                    embed = Embed(title=info.status, url=info.url,
                                  description=desc)

                    embed.set_author(name=info.display_name,
                                     url=info.url, icon_url=info.logo)
                    embed.set_thumbnail(url=info.logo)
                    embed.set_footer(text=info.url)
                    await ctx.send(embed=embed)

                else:
                    await ctx.send('%s no está en stream!' % arg)
            except IndexError:
                await ctx.send("{} no está online :(".format(arg))


def setup(client):
    client.add_cog(TWITCH(client))
