from discord.ext import commands
from math import *


class MATHS(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command(description='Python eval', aliases=['math', 'mate'])
    async def _math(self, ctx, *args):
        if args:
            arg = ''.join(args)
            await ctx.send("-> %f" % float(eval(arg)))
        else:
            await ctx.send("Dame algo :(")


def setup(client):
    client.add_cog(MATHS(client))
